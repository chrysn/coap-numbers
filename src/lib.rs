//! This crate primarily contains constants for the [CoAP] Protocol, as maintained in the [CoRE
//! Parameters] registry at IANA.
//!
//! In addition to the constants themselves, it provides functions to get their names and
//! extractable properties.
//!
//! This crate tries not to be opinionated in terms of types: It uses the Rust types that reflect
//! the possible ranges of the registry, but does not attempt to create suitable newtypes or enums
//! for the constants. That is left to downstream libraries, as they can best judge whether they
//! need to represent values that are unrecognized anyway.
//!
//! [CoAP]: http://coap.technology/
//! [CoRE Parameters]: https://www.iana.org/assignments/core-parameters/core-parameters.xhtml
//!
//! ## Features
//!
//! The only optional feature is ``alloc``, which is opt-in. It adds functions that return a
//! ``String``; those typically have formatter based functions they point to for replacement.
#![no_std]
#![cfg_attr(feature = "nightly_docs", feature(doc_auto_cfg))]

#[cfg(feature = "alloc")]
extern crate alloc;

pub mod code;
pub mod option;
pub mod signaling_option;

pub mod oscore_flag;

pub mod content_format;
