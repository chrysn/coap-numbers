//! Constants and functions for options in signaling CoAP messages (7.xx)
//!
//! Like in the options module, numbers are expressed as u16 values. The applicability of options
//! is not expressed here yet other than in the lookup, for it is unclear yet how that would be
//! usable in practice. (A program is likely to branch on the signaling code before doing any
//! option number evaluation).

// Not implemented yet in terms of macros as it's not fully clear yet how the applicability will
// generalize (and it's just a few so far)

/// Max-Message-Size (for 7.01 CSM)
pub const MAX_MESSAGE_SIZE: u16 = 2;
/// Block-Wise-Transfer (for 7.01 CSM)
pub const BLOCK_WISE_TRANSFER: u16 = 4;
/// Custody (for 7.02 Ping / 7.03 Pong)
pub const CUSTODY: u16 = 2;
/// Alternative-Address (for 7.04 Release)
pub const ALTERNATIVE_ADDRESS: u16 = 2;
/// Hold-Off (for 7.04 Release)
pub const HOLD_OFF: u16 = 4;
/// Bad-CSM-Option (for 7.05 Abort)
pub const BAD_CSM_OPTION: u16 = 2;
/// OSCORE (for any)
pub const OSCORE: u16 = 9;

/// Find the name for a CoAP signaling option when used with a particular code, if known.
///
/// Returns the registered name for an option, or None if it is not known.
///
/// ```
/// # use coap_numbers::signaling_option::{self, *};
/// use coap_numbers::code::*;
/// assert_eq!(signaling_option::to_name(CSM, MAX_MESSAGE_SIZE), Some("Max-Message-Size"));
/// assert_eq!(signaling_option::to_name(PING, 65000), None);
/// ```
#[must_use]
pub fn to_name(code: u8, option: u16) -> Option<&'static str> {
    use crate::code::{ABORT, CSM, PING, PONG, RELEASE};

    Some(match (code, option) {
        (CSM, 2) => "Max-Message-Size",
        (CSM, 4) => "Block-Wise-Transfer",
        (PING, 2) | (PONG, 2) => "Custody",
        (RELEASE, 2) => "Alternative-Address",
        (RELEASE, 4) => "Hold-Off",
        (ABORT, 2) => "Bad-CSM-Option",
        (_, 9) => "OSCORE",
        _ => {
            return None;
        }
    })
}
