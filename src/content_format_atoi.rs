// See the content_format module's documentation for how this is generated
{
if bytes_eq(content_format, b"text/plain; charset=utf-8") { return Some(0); }
if bytes_eq(content_format, b"application/cose; cose-type=\"cose-encrypt0\"") { return Some(16); }
if bytes_eq(content_format, b"application/cose; cose-type=\"cose-mac0\"") { return Some(17); }
if bytes_eq(content_format, b"application/cose; cose-type=\"cose-sign1\"") { return Some(18); }
if bytes_eq(content_format, b"application/ace+cbor") { return Some(19); }
if bytes_eq(content_format, b"image/gif") { return Some(21); }
if bytes_eq(content_format, b"image/jpeg") { return Some(22); }
if bytes_eq(content_format, b"image/png") { return Some(23); }
if bytes_eq(content_format, b"application/link-format") { return Some(40); }
if bytes_eq(content_format, b"application/xml") { return Some(41); }
if bytes_eq(content_format, b"application/octet-stream") { return Some(42); }
if bytes_eq(content_format, b"application/exi") { return Some(47); }
if bytes_eq(content_format, b"application/json") { return Some(50); }
if bytes_eq(content_format, b"application/json-patch+json") { return Some(51); }
if bytes_eq(content_format, b"application/merge-patch+json") { return Some(52); }
if bytes_eq(content_format, b"application/cbor") { return Some(60); }
if bytes_eq(content_format, b"application/cwt") { return Some(61); }
if bytes_eq(content_format, b"application/multipart-core") { return Some(62); }
if bytes_eq(content_format, b"application/cbor-seq") { return Some(63); }
if bytes_eq(content_format, b"application/edhoc+cbor-seq") { return Some(64); }
if bytes_eq(content_format, b"application/cid-edhoc+cbor-seq") { return Some(65); }
if bytes_eq(content_format, b"application/cose; cose-type=\"cose-encrypt\"") { return Some(96); }
if bytes_eq(content_format, b"application/cose; cose-type=\"cose-mac\"") { return Some(97); }
if bytes_eq(content_format, b"application/cose; cose-type=\"cose-sign\"") { return Some(98); }
if bytes_eq(content_format, b"application/cose-key") { return Some(101); }
if bytes_eq(content_format, b"application/cose-key-set") { return Some(102); }
if bytes_eq(content_format, b"application/senml+json") { return Some(110); }
if bytes_eq(content_format, b"application/sensml+json") { return Some(111); }
if bytes_eq(content_format, b"application/senml+cbor") { return Some(112); }
if bytes_eq(content_format, b"application/sensml+cbor") { return Some(113); }
if bytes_eq(content_format, b"application/senml-exi") { return Some(114); }
if bytes_eq(content_format, b"application/sensml-exi") { return Some(115); }
if bytes_eq(content_format, b"application/yang-data+cbor; id=sid") { return Some(140); }
if bytes_eq(content_format, b"application/coap-group+json") { return Some(256); }
if bytes_eq(content_format, b"application/concise-problem-details+cbor") { return Some(257); }
if bytes_eq(content_format, b"application/swid+cbor") { return Some(258); }
if bytes_eq(content_format, b"application/pkixcmp") { return Some(259); }
if bytes_eq(content_format, b"application/yang-sid+json") { return Some(260); }
if bytes_eq(content_format, b"application/ace-groupcomm+cbor") { return Some(261); }
if bytes_eq(content_format, b"application/ace-trl+cbor") { return Some(262); }
if bytes_eq(content_format, b"application/eat+cwt") { return Some(263); }
if bytes_eq(content_format, b"application/eat+jwt") { return Some(264); }
if bytes_eq(content_format, b"application/eat-bun+cbor") { return Some(265); }
if bytes_eq(content_format, b"application/eat-bun+json") { return Some(266); }
if bytes_eq(content_format, b"application/eat-ucs+cbor") { return Some(267); }
if bytes_eq(content_format, b"application/eat-ucs+json") { return Some(268); }
if bytes_eq(content_format, b"application/coap-eap") { return Some(269); }
if bytes_eq(content_format, b"application/dots+cbor") { return Some(271); }
if bytes_eq(content_format, b"application/missing-blocks+cbor-seq") { return Some(272); }
if bytes_eq(content_format, b"application/pkcs7-mime; smime-type=server-generated-key") { return Some(280); }
if bytes_eq(content_format, b"application/pkcs7-mime; smime-type=certs-only") { return Some(281); }
if bytes_eq(content_format, b"application/pkcs8") { return Some(284); }
if bytes_eq(content_format, b"application/csrattrs") { return Some(285); }
if bytes_eq(content_format, b"application/pkcs10") { return Some(286); }
if bytes_eq(content_format, b"application/pkix-cert") { return Some(287); }
if bytes_eq(content_format, b"application/aif+cbor") { return Some(290); }
if bytes_eq(content_format, b"application/aif+json") { return Some(291); }
if bytes_eq(content_format, b"application/senml+xml") { return Some(310); }
if bytes_eq(content_format, b"application/sensml+xml") { return Some(311); }
if bytes_eq(content_format, b"application/senml-etch+json") { return Some(320); }
if bytes_eq(content_format, b"application/senml-etch+cbor") { return Some(322); }
if bytes_eq(content_format, b"application/yang-data+cbor") { return Some(340); }
if bytes_eq(content_format, b"application/yang-data+cbor; id=name") { return Some(341); }
if bytes_eq(content_format, b"application/td+json") { return Some(432); }
if bytes_eq(content_format, b"application/tm+json") { return Some(433); }
if bytes_eq(content_format, b"application/uccs+cbor") { return Some(601); }
if bytes_eq(content_format, b"application/voucher+cose") { return Some(836); }
if bytes_eq(content_format, b"application/vnd.ocf+cbor") { return Some(10000); }
if bytes_eq(content_format, b"application/oscore") { return Some(10001); }
if bytes_eq(content_format, b"application/javascript") { return Some(10002); }
if bytes_eq(content_format, b"application/eat+cwt; eat_profile=\"tag:psacertified.org,2023:psa#tfm\"") { return Some(10003); }
if bytes_eq(content_format, b"application/eat+cwt; eat_profile=\"tag:psacertified.org,2019:psa#legacy\"") { return Some(10004); }
if bytes_eq(content_format, b"application/eat+cwt; eat_profile=2.16.840.1.113741.1.16.1") { return Some(10005); }
if bytes_eq(content_format, b"application/toc+cbor") { return Some(10570); }
if bytes_eq(content_format, b"application/ce+cbor") { return Some(10571); }
if bytes_eq(content_format, b"application/json@deflate") { return Some(11050); }
if bytes_eq(content_format, b"application/cbor@deflate") { return Some(11060); }
if bytes_eq(content_format, b"application/vnd.oma.lwm2m+tlv") { return Some(11542); }
if bytes_eq(content_format, b"application/vnd.oma.lwm2m+json") { return Some(11543); }
if bytes_eq(content_format, b"application/vnd.oma.lwm2m+cbor") { return Some(11544); }
if bytes_eq(content_format, b"text/css") { return Some(20000); }
if bytes_eq(content_format, b"image/svg+xml") { return Some(30000); }
}
