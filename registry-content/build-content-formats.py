#!/usr/bin/env python3

import csv
import sys

data = csv.DictReader(open(sys.argv[1]))

out_atoi = open(sys.argv[2], 'w')
out_itoa = open(sys.argv[3], 'w')

for f in (out_atoi, out_itoa):
    print("// See the content_format module's documentation for how this is generated", file=f)

print("{", file=out_atoi)

print("match content_format {", file=out_itoa)

for line in data:
    if line['Content Type'] == 'Unassigned':
        continue

    if line['Content Type'].startswith('Reserved'):
        continue

    number = int(line['ID'])
    ct = line['Content Type']
    if ' (TEMPORARY' in ct:
        ct = ct[:ct.index(' (TEMPORARY')]

    if line['Content Coding'].startswith('Content may contain arbitrary'):
        # Work around erroneous entry
        line['Content Coding'] = None

    if line['Content Coding']:
        coding = "@" + line['Content Coding']
    else:
        coding = ""
    format_with_coding = ct + coding

    # We're trusting that nothing so exotic is ever in the data that the simple
    # escaping isn't sufficient. (We're not *completely* trusting that, which
    # is why the output file is generated and committed rather than being built
    # on the fly).
    escaped_format = format_with_coding.replace('"', '\\"')

    print('if bytes_eq(content_format, b"%s") { return Some(%d); }' % (escaped_format, number), file=out_atoi)

    print('%d => Some("%s"),' % (number, escaped_format), file=out_itoa)

print("}", file=out_atoi)

print("_ => None", file=out_itoa)
print("}", file=out_itoa)
