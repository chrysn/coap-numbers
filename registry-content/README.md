The files in this directory serve as a snapshot of the relevant IANA registries.
They should be updated in the commit in which the implementation is updated,
thus making it easy to spot what needs to be changed.

IANA does not believe Copyright to be applicable,
but provides a statement placing the CSV files copied in here
under the terms of CC0 at <https://www.iana.org/help/licensing-terms>.
