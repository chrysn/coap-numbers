The coap-numbers crate provides constants used in the [CoAP] protocol
and registered in the [CoRE Parameters] registry at IANA.

Along with the numeric values,
it provides converters to string representations,
and classification in case the numbers carry inherent.

It does not provide enums or newtypes for them,
but can serve as a base to build those.
(Especially for constrained platforms,
the expectation here is that they'd have an enum of things they can work with,
and everything else is just unsupported and doesn't need to waste any more space).

[CoAP]: http://coap.technology/
[CoRE Parameters]: https://www.iana.org/assignments/core-parameters/core-parameters.xhtml

# Minimum Supported Rust Version (MSRV)

<!-- Keep in sync with Cargo.toml -->
This crate is build-tested on stable Rust 1.51.0.
It *might* compile with older versions but that may change at any time.
