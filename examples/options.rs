//! Given numbers on the command line, process them as a CoAP server would do
//!
//! Example:
//!
//! ```shell
//! $ cargo run --example options 4 6 11 11
//! ```

fn main() -> Result<(), std::num::ParseIntError> {
    let mut options = std::env::args();
    let _progname = options.next();
    let options: Vec<u16> = options.map(|a| a.parse()).collect::<Result<_, _>>()?;
    println!("Processing option numbers {:?}", options);

    let mut path = String::new();

    use coap_numbers::option;

    for o in options {
        match (o, option::get_criticality(o)) {
            (option::URI_HOST, _) => { /* Ignoring: We don't do virtual hosting here */ }
            (option::URI_PATH, _) => {
                path.push_str("/something");
            }
            (option::ETAG, _) => {
                println!("ETag was set, I might not even need to respond in the end");
            }
            (o, option::Criticality::Elective) => {
                println!("Elective option {} seen -- logging it because we might want to add support for it, but proceeding anyway", o);
            }
            (o, option::Criticality::Critical) => {
                panic!(
                    "Encountered a critical option {}, erring out with BAD_OPTION",
                    o
                );
            }
        }
    }

    println!("Message processed, request was for {}", path);

    Ok(())
}
